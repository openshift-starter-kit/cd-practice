#/bin/bash

if [ -n "${USER}" -a -n "${GITEA_HOSTNAME}" -a -n "${GITEA_TOKEN}" ]; then
  true
else
  echo "\${USER}, \${GITEA_HOSTNAME}, and \${GITEA_TOKEN} must be set." 
  echo "\${USER} is ${USER}."
  echo "\${GITEA_HOSTNAME} is ${GITEA_HOSTNAME}."
  echo "\${GITEA_TOKEN} is ${GITEA_TOKEN}."
  exit
fi

# Replace username and git credential with the environment variables.
find ../handson/ -type f -name "*.yaml" -print0 | xargs -0 sed -i "s/<USER>/${USER}/g"
find ../handson/ -type f -name "*.yaml" -print0 | xargs -0 sed -i "s/<GITEA_HOSTNAME>/${GITEA_HOSTNAME}/g"
find ../handson/ -type f -name "*.yaml" -print0 | xargs -0 sed -i "s/<GITEA_TOKEN>/${GITEA_TOKEN}/g"

# Push to remote cd-practice repository.
git config --global user.name gitea
git config --global user.email admin@gitea.com
git commit -a -m "Replaced GITEA_ info"
git push

# Create CI pipelines.
oc project ${USER}-develop
oc apply -f ../handson/pipelines/prep/gitlab-auth.yaml
oc secret link pipeline gitlab-token
oc apply -f ../handson/pipelines/prep/tekton-pvc.yaml
oc apply -f ../handson/pipelines/tasks/
oc apply -f ../handson/pipelines/handson-pipeline.yaml

# Run CI pipelines.
oc create -f ../handson/pipelines/handson-pipelinerun.yaml
